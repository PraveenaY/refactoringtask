using System;
using System.Text;


namespace FortCodeExercises.Exercise1
{
    public class Machine
    {
        /// <summary>
        /// 1. We do not need this public variable as it not being used anywhere in this class. 
        /// 2. Also this variable is never assigned but used as a conditon which could result in a bug.
        /// 3. Unnecessary memory allocation.
        /// </summary>
        //  public string machineName = ""; 


        /// <summary>
        /// 1. We have to convert this public variable into property.
        /// 2. This class needs "type" as an input value to return the machine details.
        /// </summary>
        // public int type = 0;

        /// <summary>
        /// 1. We have created a new property "Type", which can be used instead of "type"
        /// </summary>
        public int Type { get; set; } = 0;

        /// <summary>
        /// Naming Convention: Property name should be Camel Case
        /// </summary>
        public string Name
        {
            get
            {
                string machineName;
                switch (this.Type)
                {
                    case 0:
                        machineName = "bulldozer";
                        break;
                    case 1:
                        machineName = "crane";
                        break;
                    case 2:
                        machineName = "tractor";
                        break;
                    case 3:
                        machineName = "truck";
                        break;
                    case 4:
                        machineName = "car";
                        break;
                    default:
                        machineName = string.Empty;
                        break;
                }
                return machineName;
            }
        }

        /// <summary>
        /// Naming Convention: Property name should be Camel Case
        /// </summary>
        public string Description
        {
            get
            {
                bool hasMaxSpeed;//no need to assign default value, we do it in switch default
                //also the if else can be converted to switch for performance and readability
                switch (this.Type)
                {
                    case 3:
                    case 4:
                        hasMaxSpeed = false;
                        break;
                    default:
                        hasMaxSpeed = true;
                        break;
                }
                return String.Format(" {0} {1}  [{2}].", Color, Name, this.GetMaxSpeed(this.Type, hasMaxSpeed));
                //StringBuilder maxSpeedDescription = new StringBuilder();
                //maxSpeedDescription.Append(" ");
                //maxSpeedDescription.Append(Color);
                //maxSpeedDescription.Append(" ");
                //maxSpeedDescription.Append(Name);
                //maxSpeedDescription.Append(" [");
                //maxSpeedDescription.Append(this.GetMaxSpeed(this.Type, hasMaxSpeed));
                //maxSpeedDescription.Append("].");
                //return maxSpeedDescription.ToString(); ;
            }
        }

        /// <summary>
        /// Naming Convention: Property name to Camel Case
        /// if else to switch
        /// </summary>
        public string Color
        {
            get
            {
                string color;
                switch (this.Type)
                {
                    case 0:
                        color = "red";
                        break;
                    case 1:
                        color = "blue";
                        break;
                    case 2:
                        color = "green";
                        break;
                    case 3:
                        color = "yellow";
                        break;
                    case 4:
                        color = "brown";
                        break;
                    default:
                        color = "white";
                        break;
                }
                return color;
            }
        }

        /// <summary>
        /// Naming Convention: Property name should be Camel Case
        /// here base color values and color property values are same.
        /// we can use the Color class property 
        /// </summary>
        public string TrimColor
        {
            get
            {
                switch (this.Type)
                {
                    case 1:
                        return IsDark(this.Color) ? "black" : "white";
                    case 2:
                        return IsDark(this.Color) ? "gold" : string.Empty;
                    case 3:
                        return IsDark(this.Color) ? "silver" : string.Empty;
                    default:
                        return string.Empty;
                }
            }
        }

        /// <summary>
        /// Naming Convention: Property name should be Camel Case
        /// If else to switch
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public bool IsDark(string color)
        {
            bool isDark;
            switch (color)
            {
                case "red":
                case "green":
                case "black":
                case "crimson":
                    isDark = true;
                    break;
                default:
                    isDark = false;
                    break;
            }
            return isDark;
        }

        /// <summary>
        /// Naming Convention: Property name should be Camel Case
        /// </summary>
        /// <param name="machineType"></param>
        /// <param name="noMax"></param>
        /// <returns></returns>
        public int GetMaxSpeed(int machineType, bool noMax = false)
        {
            var max = 70;
            switch (machineType)
            {
                case 0:
                    max = noMax == true ? 80 : max;
                    break;
                case 1:
                    max = noMax == true ? 75 : max;
                    break;
                case 2:
                case 4:
                    max = noMax == true ? 90 : max;
                    break;
                default:
                    return max;
            }
            return max;
        }
    }
}