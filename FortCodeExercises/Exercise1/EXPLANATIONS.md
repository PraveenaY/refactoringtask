step 1) got the code from relocated repository git clone https://bitbucket.org/hriinc/eval-fast-bk-2-fw3e.git

1. We do not need this public variable as it not being used anywhere in this class. 
2. Also this variable is never assigned but used as a conditon which could result in a bug.
3. Unnecessary memory allocation.
public string machineName = ""; 


1. We have to convert this public variable into property.
2. This class needs "type" as an input value to return the machine details.
public int type = 0;


1. We have created a new property "Type", which can be used instead of "type" class variable
public int Type { get; set; } = 0;


-> All Class Property names should be Camel Case
-> Converted all if-else statements to switch case
-> Replaced string concatenations(+=) to string format for readability. could also use StringBuilder if the string concatenation operations are extensive.
-> Removed some redundant code in IsDark() method.